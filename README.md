# My Configuration for VIM

## Install Vundle plugin manager
Vun
dle is required to install pligins with vim.

Follow the instructions in the readme to install vundle

https://github.com/VundleVim/Vundle.vim

## Install plugins
Use the website https://vimawesome.com/ to search for plugins. 

https://vimawesome.com/plugin/youcompleteme
https://vimawesome.com/plugin/python-vim-red
https://vimawesome.com/plugin/markdown-syntax

## Configure netrw
Netrw is a file explorer that is built into vim.

The last few lines in the .vimrc file are for configuring netrw
